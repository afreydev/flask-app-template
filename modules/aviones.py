import functools
from flask import Blueprint, request, jsonify
from models import avion
import db


bp = Blueprint('aviones', __name__, url_prefix='/aviones')

@bp.route('/', methods=['GET'])
def list():
    avion.listar()
    return "Este es el servicio que lista los aviones"

@bp.route('/<int:avion>', methods=['GET'])
def get(avion):
    print(db.get_connection())
    return "Este es el servicio que retorna el avion: {}".format(avion)

@bp.route('/<int:avion>', methods=['PUT'])
def update(avion):
    print(db.get_connection())
    return "Este es el servicio que actualiza el avion: {}".format(avion)

@bp.route('/<int:avion>', methods=['DELETE'])
def delete(avion):
    print(db.get_connection())
    return "Este es el servicio que borra el avion: {}".format(avion)
