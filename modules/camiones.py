import functools
from flask import Blueprint, request, jsonify
import db

bp = Blueprint('camiones', __name__, url_prefix='/camiones')

@bp.route('/', methods=['GET'])
def list():
    print(db.get_connection())
    return "Este es el servicio que lista los camiones"

@bp.route('/<int:camion>', methods=['GET'])
def get(camion):
    print(db.get_connection())
    return "Este es el servicio que retorna el camion: {}".format(camion)

@bp.route('/<int:camion>', methods=['PUT'])
def update(camion):
    print(db.get_connection())
    return "Este es el servicio que actualiza el camion: {}".format(camion)

@bp.route('/<int:camion>', methods=['DELETE'])
def delete(camion):
    print(db.get_connection())
    return "Este es el servicio que borra el camion: {}".format(camion)
