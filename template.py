from flask import Flask

def create_app(test_config=None):   
    app = Flask(__name__, instance_relative_config=True)
        
    from modules.aviones import bp as bpaviones
    from modules.camiones import bp as bpcamiones
    app.register_blueprint(bpaviones)
    app.register_blueprint(bpcamiones)

    return app
